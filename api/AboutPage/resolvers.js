import About from './About';

export default {
    Query: {
        AboutQuery(obj, args, { userId }) {
            return About.find({})
        },
        AboutHomeQuery(obj, args, { userId }) {
            return About.find({"visible": {$eq: "public"} });
        },
    },

    Mutation: {
        createAbout(obj, { aboutText, visible }, { userId } ) {
            if (userId) {
                const AboutId = About.insert({
                    aboutText,
                    visible,
                    userId: userId,
                    TimeStamp: new Date()

                });
                return About.findOne(AboutId);
            }
            throw new Error("Unauthorized");
        },
        updateAbout(obj, { _id, aboutText, visible }, { userId } ) {
            if (userId) {
                const AboutId = About.update({_id: _id}, {$set: { "aboutText": aboutText, "visible": visible } });

                return About.findOne(AboutId);
            }
            throw new Error("Unauthorized");
        }
    }
};