import { Mongo } from 'meteor/mongo';

const Deals = new Mongo.Collection("deals");

export default Deals;