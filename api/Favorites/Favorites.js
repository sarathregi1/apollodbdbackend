import { Mongo } from 'meteor/mongo';

const Favorites = new Mongo.Collection("favorites");

export default Favorites;