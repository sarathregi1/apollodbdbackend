import CuisineLists from "./CuisineLists";
import deburr from 'lodash/deburr';

export default {
    Query: {
        cuisine(obj, args, { userId }) {
            return CuisineLists.find({});
        },
        cuisineLists(parent, {name}) {
            const inputValue = deburr(name.trim()).toLowerCase();
            return CuisineLists.find( { "name": { $regex: ".*" + inputValue + ".*" } } )
        },
        cuisineListsWithoutInput(obj, args,{userId}){
            return CuisineLists.find({});
        }
    },

    Mutation: {
        createCuisineList(obj, { name, TimeStamp }, { userId } ) {
            if (userId) {
                const cuisinelistId = CuisineLists.insert({
                    name,
                    TimeStamp
                });
                return CuisineLists.findOne(cuisinelistId);
            }
            throw new Error("Unauthorized");
        }
    }
};