// import User from './User';

export default {
    Query: {
        user(obj, args, { user }) {
            return user || {};
        },
        userInfoQuery(obj, {_id}) {
            return Meteor.users.find({"_id": {$eq: _id}});
        }
    },
    User: {
        email: user => user.emails[0].address,
    }
};