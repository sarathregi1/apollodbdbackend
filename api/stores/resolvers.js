import Stores from "./stores";

export default {
    Query: {
        stores(obj, args, { userId }) {
            return Stores.find({}).fetch();
        },
        storeinfoQuery: (parent, {_id}) => (
            Stores.find({ "_id": { $eq: _id } })
        ),
        storeHomeQuery: (parent, {StoreAddress, StorePlaceId}) => (
            Stores.find({ $or: [{"StoreAddress": { $eq: StoreAddress }}, {"StorePlaceid": { $eq: StorePlaceId }}] })
        )
    },

    Mutation: {
        createStore(obj, { StorePlaceId, StoreName, StorePhone, StoreHours, Website, StoreAddress, TimeStamp, PriceLevel, Photo }, { userId } ) {
            if (userId) {
                const storeId = Stores.update(
                    { "StorePlaceId": { $eq:  StorePlaceId } },
                    {
                    userId,
                    StorePlaceId,
                    StoreName,
                    StorePhone,
                    StoreHours,
                    Website,
                    StoreAddress,
                    PriceLevel,
                    TimeStamp,
                    Photo
                    },
                    { upsert: true }
                );
                return Stores.findOne(storeId);
            }
            throw new Error("Unauthorized");
        }
    }
};