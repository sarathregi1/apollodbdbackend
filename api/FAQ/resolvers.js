import FAQ from "./FAQ";

export default {
    Query: {
        FAQQuery(obj, args, { userId }) {
            return FAQ.find({})
        },
        FAQHomeQuery(obj, args, { userId }) {
            return FAQ.find({"visible": {$eq: "public"} });
        },
    },

    Mutation: {
        createFAQ(obj, { question, answer, visible }, { userId } ) {
            if (userId) {
                const FAQId = FAQ.insert({
                    question,
                    answer,
                    visible,
                    userId: userId,
                    TimeStamp: new Date()

                });
                return FAQ.findOne(FAQId);
            }
            throw new Error("Unauthorized");
        },
        updateFAQ(obj, { _id, question, answer, visible }, { userId } ) {
            if (userId) {
                const FAQId = FAQ.update({_id: _id}, {$set: { "question": question, "answer": answer, "visible": visible } });

                return FAQ.findOne(FAQId);
            }
            throw new Error("Unauthorized");
        },
        removeFAQ(obj, { _id }, { userId } ) {
            if (userId) {
                const FAQId = FAQ.remove({
                    _id
                });
                return FAQ.findOne(FAQId);
            }
            throw new Error("Unauthorized");
        }
    }
};