import { ApolloServer } from "apollo-server-express";
import { makeExecutableSchema } from "graphql-tools";
import merge from 'lodash/merge';
import { WebApp } from 'meteor/webapp'
import { getUser } from 'meteor/apollo'
import DealSchema from '../api/deals/Deals.graphql';
import DealsResolvers from '../api/deals/resolvers';
import DealTypeSchema from '../api/DealType/DealType.graphql';
import DealTypeResolvers from '../api/DealType/resolvers';
import UsersSchema from '../api/users/User.graphql';
import UsersResolvers from '../api/users/resolvers';
import StoresSchema from '../api/stores/Stores.graphql';
import StoresResolvers from '../api/stores/resolvers';
import CuisineListSchema from '../api/CuisineList/CuisineList.graphql'
import CuisineListResolvers from '../api/CuisineList/resolvers';
import FavoritesSchema from '../api/Favorites/Favorites.graphql'
import FavoritesResolvers from '../api/Favorites/resolvers';
import WelcomeSchema from '../api/WelcomeSection/WelcomeSection.graphql';
import WelcomeResolvers from '../api/WelcomeSection/resolvers';
import FAQSchema from '../api/FAQ/FAQ.graphql';
import FAQResolvers from '../api/FAQ/resolvers';
import AboutSchema from '../api/AboutPage/About.graphql';
import AboutResolvers from '../api/AboutPage/resolvers';
// dgjddsdddddddsacdxddsdsdsgdkgjdgdddsdddfddgddsdhgddssc

const typeDefs = [
    DealSchema,
    UsersSchema,
    StoresSchema,
    CuisineListSchema,
    DealTypeSchema,
    FavoritesSchema,
    WelcomeSchema,
    FAQSchema,
    AboutSchema
];

const resolvers = merge( 
    DealsResolvers,
    UsersResolvers,
    StoresResolvers,
    CuisineListResolvers,
    DealTypeResolvers,
    FavoritesResolvers,
    WelcomeResolvers,
    FAQResolvers,
    AboutResolvers
);

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

// const oidc = new Provider({
// });

const server = new ApolloServer({
    schema,
    context: async ({ req }) => ({
        user: await getUser(req.headers.authorization)
    })
});

// oidc.applyMiddleware({
//     path: '/'
// })

server.applyMiddleware({
    app: WebApp.connectHandlers,
    path: '/graphql'
})

  
WebApp.connectHandlers.use('/graphql', (req, res) => {
    if (req.method === 'GET') {
      res.end()
    }
})


//   server.listen().then(({ url }) => {
//     console.log(`🚀 Server ready at ${url}`)
//   });
  

// createApolloServer({ 
//     schema, 
// });

